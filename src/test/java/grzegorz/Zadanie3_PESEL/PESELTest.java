package grzegorz.Zadanie3_PESEL;

import grzegorz.zadanie3_PESEL.PESEL;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

public class PESELTest {

    private  PESEL pesel;

    @Before
    public void setup(){pesel = new PESEL();}

    @Test
    public void isTrue(){
        Assertions.assertThat(pesel.isValid("11040450133")).isEqualTo(true);
    }

    @Test
    public void isFalse(){
        Assertions.assertThat(pesel.isValid("11040450132")).isEqualTo(false);
    }

    @Test
    public void isNull(){
        Assertions.assertThat(pesel.isValid(null)).isNotNull().isEqualTo(false);
    }

    @Test
    public void isEmpty(){
        Assertions.assertThat(pesel.isValid("")).isNotNull().isEqualTo(false);
    }

    @Test
    public void isTooShort(){
        Assertions.assertThat(pesel.isValid("11040")).isNotNull().isEqualTo(false);
    }

    @Test (expected = NumberFormatException.class)
    public void wrongNumber(){
        Assertions.assertThat(pesel.isValid("1104045013A"));
    }
}
