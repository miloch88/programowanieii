package grzegorz.Zadanie2;

import grzegorz.zadanie2_ID.ID;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

public class IDTest {

    private ID id;

    @Before
    public void setup(){id = new ID();}

    @Test
    public void isTrue(){
        Assertions.assertThat(id.vailidID("WHU915897")).isEqualTo(true);
    }

    @Test
    public void isNull(){
        Assertions.assertThat(id.vailidID(null)).isNotNull().isEqualTo(false);
    }

    @Test
    public void isEmpty(){
        Assertions.assertThat(id.vailidID("")).isNotNull().isEqualTo(false);
    }

    @Test
    public void isTooShort(){
        Assertions.assertThat(id.vailidID("ABA3")).isNotNull().isEqualTo(false);
    }

    @Test
    public void wrongNumber(){
        Assertions.assertThat(id.vailidID("WHUAA5897")).isEqualTo(false);
    }


}
