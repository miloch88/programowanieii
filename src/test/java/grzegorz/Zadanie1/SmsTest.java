package grzegorz.Zadanie1;

import grzegorz.zadanie1.Sms;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

public class SmsTest {

    private Sms sms;
    private String message = "Ala ma kota";

    @Before
    public void setup() {
        sms = new Sms();
    }

    @Test
    public void testStringIsNull() {
        String result = sms.loadSMS("Ala ma kota");
        Assertions.assertThat(result).isNotEmpty().isNotEmpty().isEqualTo("AlaMaKota");
    }

    @Test
    public void countityMessages(){
        Assertions.assertThat(sms.costSMS("gfdsxfcghjkliouytrdsxfcvghbjnklkoiuytrsdxcvbhnjkoiuytrdxfcvgbhnjkijuytrdexfcvbnjkiuytrdxfcvbngfdsxfcghjkliouytrdsxfcvghbjnklkoiuytrsdxcvbhnjgfdsxfcghjkliouytrdsxfcvghbjnklkoiuytrsdxcvbhnjkoiuytrdxfcvgbhnjkijuytrdexfcvbnjkiuytrdxfcvbngfdsxfcghjkliouytrdsxfcvghbjnklkoiuytrsdxcvbhnjk")).isEqualTo(3);
    }


}
