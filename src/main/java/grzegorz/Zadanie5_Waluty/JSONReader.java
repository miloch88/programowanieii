package grzegorz.Zadanie5_Waluty;

import com.google.gson.Gson;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDate;
import java.util.Scanner;

public class JSONReader {

    public static void getValue100UnitsMid(String url) throws IOException {
        Currency currency = readJsonFormUrl(url);

        System.out.printf("Aktualny kurs %s na dzień: %s to: %.4f, a 100 PLN jest warte %.2f PLN \n",
                currency.getCode(),
                currency.rates[0].getEffectiveDate(),
                currency.rates[0].getMid(),
                currency.rates[0].getMid() * 100);
    }

    public static void getValue100UnitsAsk(String url) throws IOException {
        Currency currency = readJsonFormUrl(url);

        System.out.printf("Aktualny kurs sprzedaży %s na dzień: %s to: %.4f, a 100 PLN jest warte %.2f PLN \n",
                currency.getCode(),
                currency.rates[0].getEffectiveDate(),
                currency.rates[0].getAsk(),
                currency.rates[0].getAsk() * 100);
    }

//    public static String createDate() {
//
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Wybierz swoją walutę (USD, EUR, GBP, CHF): ");
//        String currency = scanner.nextLine().toLowerCase();
//        System.out.println("Wybierz rodzaj kursu (sredni, kupno, sprzedaz");
//        String rate = scanner.nextLine().toLowerCase();
//        switch(rate){
//            case ("sredni"){
//
//            }
//
//        }
//
//        String stringUrl = "http://api.nbp.pl/api/exchangerates/rates/c/usd/";
//        stringUrl += date.minusMonths(1);
//        System.out.println(stringUrl);
//    }

    public static void perviousMount() throws IOException {
        LocalDate dateNow = LocalDate.now().minusDays(2);
        LocalDate datePrevious = LocalDate.now().minusMonths(1);
        double now, previous;

        //Zabezpieczenie na Łikęd
        try {
            now = readJsonFormUrl("http://api.nbp.pl/api/exchangerates/rates/c/eur/" + dateNow).rates[0].getBid();
        } catch (FileNotFoundException e) {
            try {
                now = readJsonFormUrl("http://api.nbp.pl/api/exchangerates/rates/c/eur/" + dateNow.minusDays(1)).rates[0].getBid();
            } catch (FileNotFoundException ex) {
                now = readJsonFormUrl("http://api.nbp.pl/api/exchangerates/rates/c/eur/" + dateNow.minusDays(2)).rates[0].getBid();
            }
        }

        try {
            previous = readJsonFormUrl("http://api.nbp.pl/api/exchangerates/rates/c/eur/" + datePrevious).rates[0].getAsk();
        } catch (FileNotFoundException e) {
            try {
                previous = readJsonFormUrl("http://api.nbp.pl/api/exchangerates/rates/c/eur/" + datePrevious.minusDays(1)).rates[0].getAsk();
            } catch (FileNotFoundException ex) {
                previous = readJsonFormUrl("http://api.nbp.pl/api/exchangerates/rates/c/eur/" + datePrevious.minusDays(2)).rates[0].getAsk();
            }
        }
        System.out.printf("Nasz zysk/strata miesięczna wynosi: %.2f PLN", (((100/previous)*now))-100);
    }


    public static Currency readJsonFormUrl(String url) throws IOException {

        URLConnection connection = new URL(url).openConnection();
        InputStream is = connection.getInputStream();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String jsonTxt = br.readLine();

            Gson gson = new Gson();
            Currency currency = gson.fromJson(jsonTxt, Currency.class);

            return currency;
        } finally {
            is.close();
        }
    }

}
