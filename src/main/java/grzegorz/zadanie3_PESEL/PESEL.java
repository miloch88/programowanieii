package grzegorz.zadanie3_PESEL;

public class PESEL {

    private static final int[] FACTORS = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3};

    public  boolean isValid(String pesel){
        if(pesel == null || pesel.length() != 11){
            return false;
        }

        char[] charSplited = pesel.toCharArray();
        int sum = 0;
        for (int i = 0; i < charSplited.length-1; i++) {
            if(!Character.isDigit(charSplited[i])){
                throw new NumberFormatException("");
            }
            sum += FACTORS[i] * Character.getNumericValue(charSplited[i]);
        }

        int lastNo = 10 - sum%10;
        return lastNo == Integer.parseInt(String.valueOf(charSplited[charSplited.length-1]));
    }
}


