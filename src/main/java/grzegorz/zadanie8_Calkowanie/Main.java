package grzegorz.zadanie8_Calkowanie;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static grzegorz.zadanie8_Calkowanie.Integral.calculateIntegral;

public class Main {
    public static void main(String[] args) {

        System.out.println(calculateIntegral(0, 15, 10000));

        ExecutorService threadPool = Executors.newFixedThreadPool(4);

        double totalArea = 0.0;
        for(int i = 0; i < 15; i++){
            double smallArea = calculateIntegral(i, i+1, 1000);
            totalArea += smallArea;
        }

        System.out.println(totalArea);

    }

}
