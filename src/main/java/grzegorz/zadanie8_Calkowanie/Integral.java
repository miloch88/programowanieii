package grzegorz.zadanie8_Calkowanie;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Integral {

    private static double function(double x) {
        return (3 * Math.sin(x)) - (0.2 * Math.pow(x, 3)) + (3 * Math.pow(x, 2));
    }

    static double calculateIntegral(double xp, double xk, int n) {
        long startTime = System.currentTimeMillis();
        double sum = 0.0;
        double base = (xk-xp)/n; // dokładność (ilość części)

//        for(double x = xp + base/2; x< xk; x+=base);
        for (int i = 0; i < n; i++){
            double x = xp+ base/2+ i*base;
            double height = function(x);
            double area = base * height;
            sum += area;
        }

//        double dx = (xk - xp)/(double)n;
//        double sum = 0;
//        for(int i = 1; i<=n; i++){
//            sum += function(xp+ i* dx);
//        }
//        sum *= dx;

//        Moje kalkulacje
//        for (double i = 0.0000005; i <= 15; i += 0.000001) {
//            sum += function(i - 0.0000005) * 0.000001;
//        }

        long stopTime = System.currentTimeMillis();

        System.out.println("Czas obliczeń: " + (stopTime - startTime));
        return sum;

    }
}
