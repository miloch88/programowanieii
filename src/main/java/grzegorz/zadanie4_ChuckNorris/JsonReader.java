/*
https://stackoverflow.com/questions/4308554/simplest-way-to-read-json-from-a-url-in-java#
 */

package grzegorz.zadanie4_ChuckNorris;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Set;

public class JsonReader {

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static Joke readJsonFromUrl(String url) throws IOException, JsonIOException {
       //Otwieramy połączenie
        URLConnection connection = new URL(url).openConnection();
        //Musimy dodać dodatkowe dane na temat agent i przedkądarki (Narzędzia dla programistów)
        connection.addRequestProperty("User-agent", "Chrome");
        //zpaisujemy aby BufferReader mógł oczytać
        InputStream is = connection.getInputStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            //Nowy obiekt Gson
            Gson gson = new Gson();
            //Zamiana na Joke
            Joke joke = gson.fromJson(jsonText, Joke.class);
            return joke;
        } finally {
            is.close();
        }


    }

    public static void main(String[] args) throws IOException {
        /*
        Rozwiązanie Grzegorza:
        URL url = new URL("https://api.chicknorris.io/jokes/random");
        URLConnection connection = url.openConnection();
        connection.addRequestProperty("User-Agent", "Chrome")
        InputStream inputStream = connection.getInpustStream();
        BufferReader br = new BufferReader(inputStream));
        String jsonText = br.readline();
        Gson gson = new Gson();
        Joke joke = gdon.formJson(jsonText, Joke.class)
        System.out.pruntln(joke.value)
         */

        Set<Joke> tenJokes = new HashSet<>();
        for (int i = 0; i < 10; i++) {
        Joke json = readJsonFromUrl("https://api.chucknorris.io/jokes/random");
        tenJokes.add(json);
//        System.out.println(json.value);

        }
        System.out.println(tenJokes);

//        System.out.println(json.id);
//        System.out.println(json.icon_url);
//        System.out.println(json.category);
//        System.out.println(json.url);
    }
}
