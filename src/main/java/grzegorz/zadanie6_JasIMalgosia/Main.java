package grzegorz.zadanie6_JasIMalgosia;

import java.util.concurrent.CountDownLatch;

public class Main {

//    Tutaj możemy stworzyć countDownLatch

    public static void main(String[] args) throws InterruptedException {


        System.out.println(Runtime.getRuntime().availableProcessors());

        CountDownLatch countDownLatch = new CountDownLatch(2);

        Thread jas = new Thread(new Jas(countDownLatch));
        Thread malgosia = new Thread(new Malgosia(countDownLatch));

//        .run() uruchamia wątek jeden po drugim
        jas.start();
        malgosia.start();

        countDownLatch.await();

//        Bez .join() by jas.start(), malgosia.start() i Koniec świata jest uruchamiane jdnoscześnie
//        jas.join();
//        malgosia.join();

        System.out.println("Koniec dnia");

    }
}
