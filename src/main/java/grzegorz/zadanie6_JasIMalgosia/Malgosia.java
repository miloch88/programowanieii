package grzegorz.zadanie6_JasIMalgosia;

import java.util.concurrent.CountDownLatch;

public class Malgosia implements Runnable {

    CountDownLatch countDownLatch;

    public Malgosia(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void run() {


        try {
            System.out.println("Małgosia zczyna biegać..");
            Thread.sleep(6000);
            System.out.println("Małgosia skończyła biegać!");

            System.out.println("Małgosia zaczyna brać prysznic...");
            Thread.sleep(2000);
            System.out.println("Małgosia skończyła prysznic");

            System.out.println("Małgosia zaczyna jeść śniadanie...");
            Thread.sleep(1000);
            System.out.println("Małgosia skończyła jeść śniadanie");

            System.out.println("Małgosia zaczęła się ubierać...");
            Thread.sleep(1000);
            System.out.println("Małgosia jest już ubrana");

            System.out.println("Małgosia wychodzi na spotkanie z koleżanką...");
            Thread.sleep(25000);
            System.out.println("Małgosia wróciła do domu!");

            countDownLatch.countDown();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
