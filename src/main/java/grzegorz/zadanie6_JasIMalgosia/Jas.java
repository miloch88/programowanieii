package grzegorz.zadanie6_JasIMalgosia;

import java.util.Collections;
import java.util.concurrent.CountDownLatch;

public class Jas implements Runnable {

    CountDownLatch countDownLatch;

    public Jas(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void run() {
        try {
            System.out.println("Jaś przygtwouje śniadanie...");
            Thread.sleep(5000);
            System.out.println("Jaś skończył jeść śniadanie!");

            System.out.println("Jaś zczyna prysznic...");
            Thread.sleep(3000);
            System.out.println("Jaś skończył prysznic!");

            System.out.println("Jaś zaczyna się ubierać...");
            Thread.sleep(1000);
            System.out.println("Jaś jest już ubrany!");

            System.out.println("Jaś wychodzi na zakupy...");
            Thread.sleep(15000);
            System.out.println("Jaś wrócił z zakupów!");

            System.out.println("Jaś zaczyna grać na konsoli...");
            Thread.sleep(5000);
            System.out.println("Jaś skończył grać na konsoli!");

            countDownLatch.countDown();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

