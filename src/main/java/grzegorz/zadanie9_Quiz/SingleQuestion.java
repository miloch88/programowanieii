package grzegorz.zadanie9_Quiz;

import java.util.ArrayList;
import java.util.List;

public class SingleQuestion {

    private String question = null;
    private List<String> answers = new ArrayList<>();

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }

}
