package grzegorz.zadanie9_Quiz;

import java.io.IOException;
import java.util.*;

import static grzegorz.zadanie9_Quiz.CategoryQuestions.readAllCategoriesQuestions;

public class Main {
    public static void main(String[] args) throws IOException {

        Map<String, List<SingleQuestion>> categories = readAllCategoriesQuestions();

        System.out.println("Wybierz kategorię: ");
        ArrayList<String> categoryNames = new ArrayList<>(categories.keySet());

        //Tworzymy listę do wyświetlenia np.: > (16) Sports
        for (int i = 0; i < categoryNames.size(); i++) {
            String categoryName = categoryNames.get(i);
            System.out.println("> (" + (i+1) + ") " + categoryName);
        }
        Scanner scanner = new Scanner(System.in);
        int userCategoryChoice = scanner.nextInt();

        String selectedCategoryName = categoryNames.get(userCategoryChoice - 1);
        List<SingleQuestion> selectedCategoryQuestions = categories.get(selectedCategoryName);

        Collections.shuffle(selectedCategoryQuestions);

        int points = 0;
        for (int i = 0; i < 10; i++) {
            SingleQuestion quizQuestions = selectedCategoryQuestions.get(i);
            System.out.println("Pytanie " + (i+1) + ": " + quizQuestions.getQuestion());
            String goodAnswer = quizQuestions.getAnswers().get(0);
            Collections.shuffle(quizQuestions.getAnswers());

            for (int j = 0; j < quizQuestions.getAnswers().size(); j++) {
                String answer = quizQuestions.getAnswers().get(j);
                System.out.println(" " + (j+1) + ") " + answer);
            }
            System.out.println("Wybrana odpowiedź: ");
            int selectedAnswer = scanner.nextInt();
            if (quizQuestions.getAnswers().get(selectedAnswer - 1).equals(goodAnswer)){
                System.out.println("Dobra odpowiedź!");
                points++;
            }else{
                System.out.println("Błędna odpowiedź, prawidłowa to : " + goodAnswer);
            }

        }

            System.out.println("Koniec, zdobyłeś " + points + "/10 punktów");

    }
}
