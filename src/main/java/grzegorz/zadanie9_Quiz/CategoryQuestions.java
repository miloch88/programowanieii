package grzegorz.zadanie9_Quiz;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.*;
import java.util.*;


public class CategoryQuestions {

    //czytamy wszystkie String - kategorie pytań, List - Single Questions;
//    Map<String, List<SingleQuestion>> categories = readAllCategoriesQuestions();

    public static Map<String, List<SingleQuestion>> readAllCategoriesQuestions() {

        Map<String, List<SingleQuestion>> categories = new HashMap<>();

        File folder = new File("src\\main\\resources\\zadanie8_Quiz");
        File[] allFiles = folder.listFiles();
        for (File f : allFiles) {
            List<SingleQuestion> categoryQuestions = readAllQuizQuestionsFromFile(f);

            String categoryName = f.getName().replaceAll(".txt", "")
                    .replaceAll("_", " ");
            categories.put(categoryName, categoryQuestions);
        }
        return categories;
    }

    private static List<SingleQuestion> readAllQuizQuestionsFromFile(File file) {

        List<SingleQuestion> allQuestions = new ArrayList<>();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                SingleQuestion sq = new SingleQuestion();
                sq.setQuestion(line);
//                String answerNumberString = br.readLine();
                int answerNumber = Integer.parseInt(br.readLine());
                for (int i = 0; i < answerNumber; i++) {
                    sq.getAnswers().add(br.readLine());
                }
                allQuestions.add(sq);
            }
        } catch (FileNotFoundException e) {
            System.err.println("Wczytywanie pliku " + file.getName() + " nie powiodło się");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return allQuestions;
    }

//    List<SingleQuestion> questions = new ArrayList<>();
//        Set<SingleQuestion> set = new HashSet<>();
//
//    void readQuestions(String link) throws IOException {
//        String line;
//        BufferedReader br = new BufferedReader(new FileReader(link));
//        while ((line = br.readLine()) != null) {
//            SingleQuestion singleQuestion = new SingleQuestion();
//            singleQuestion.setQuestion(line);
//            int number = Integer.parseInt(line = br.readLine());
//            for (int i = 0; i < number; i++) {
//                singleQuestion.getAnswers().add(line = br.readLine());
//            }
//            questions.add(singleQuestion);
//        }
//    }

//    void drawQuestions() {
//        do {
//            Random random = new Random();
//            set.add(questions.get(random.nextInt(questions.size())));
//        } while (set.size() != 10);
//    }


}



