package grzegorz.zadanie7_LiczbyPierwsze;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class Prime {

    public static boolean isPrime(long number) {
        if (number < 1) {
            return false;
        } else {

            for (int i = 2; i < number; i++) {
                if (number % i == 0) {
                    return false;
                }
            }
            return true;
        }
    }

    private static AtomicInteger primeCount = new AtomicInteger();

    public static void readAndCheck(String string) throws InterruptedException {

        long startTime = System.currentTimeMillis();

        ExecutorService threadPool = Executors.newFixedThreadPool(16);

        final CountDownLatch latch = new CountDownLatch(5000);

        try {
            BufferedReader br = new BufferedReader(new FileReader(string));
            String line;
            while ((line = br.readLine()) != null) {
                final int number = Integer.parseInt(line);
                threadPool.submit(new Runnable() {
                    @Override
                    public void run() {

                        if (isPrime(number)) {
                            primeCount.incrementAndGet();
                        }
                        latch.countDown();
                    }
                });
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        latch.await();
        threadPool.shutdown();

        long stopTime = System.currentTimeMillis();

        System.out.println("Znaleziono liczby pierwszych: " + primeCount);
        System.out.println("Czas potrzebny na obliczenia: " + (stopTime - startTime));

    }
}
