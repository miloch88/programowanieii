package grzegorz.zadanie3_CoToZaJezyk;

import com.detectlanguage.DetectLanguage;
import com.detectlanguage.Result;
import com.detectlanguage.errors.APIError;


import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class Main {
    public static void main(String[] args) {
        DetectLanguage.apiKey = "243f50ea725ee7d700ba16cb1d461692";
        String string = "";
        String linia;

        File folder = new File("src\\main\\resources\\zadanie3_CoToZaJezyk");
        File[] list = folder.listFiles();

        try {
            for (int i = 0; i < list.length; i++) {

                BufferedReader bufferedReader = new BufferedReader(new FileReader(list[i]));

                while ((linia = bufferedReader.readLine()) != null) {
                    string += " " + linia;
                }

                List<Result> results = DetectLanguage.detect(string);
                Result result = results.get(0);

  //Klasa Locale aby wyświetlić japoński, polski, angielski czy niemiecki; zależy to od języka systemu operacyjnego
            System.out.println(new Locale(result.language).getDisplayLanguage());
                string = null;
            }

        } catch (APIError apiError) {
            apiError.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

