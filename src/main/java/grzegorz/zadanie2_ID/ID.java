package grzegorz.zadanie2_ID;

import java.util.ArrayList;
import java.util.List;

public class ID {

    List<Integer> list = new ArrayList<Integer>();
    char[] chars = new char[9];
    int suma = 0;


    public boolean vailidID(String ID) {
        if(ID == null || ID.length() != 9){
            return false;
        }

        chars = ID.toCharArray();
        for (char ch : chars) {
            list.add(Character.getNumericValue(ch));
        }

        suma = list.get(0)*7 + list.get(1)*3 + list.get(2)*1 + list.get(3)*9 + list.get(4)*7 + list.get(5)*3 +
                list.get(6)*1 + list.get(7)*7 + list.get(8)*3;

        if(suma % 10 == 0){
            list.clear();
            return true;


        }else{
            list.clear();
            return false;
        }


    }


}
