package grzegorz.zadanie10_Koty;

import com.google.gson.Gson;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;

public class Cat {

    private String file;

    public String getFile() {
        return file;
    }

    public static Cat readApi() throws IOException {

        URL url = new URL("https://aws.random.cat/meow");
        URLConnection connection = url.openConnection();
        InputStream is = connection.getInputStream();

        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String jsonTxt = br.readLine();

        Gson gson = new Gson();
        Cat cat = gson.fromJson(jsonTxt, Cat.class);

        return cat;
    }

    public static void downloadCats() throws IOException {

        for (int i = 0; i < 10; i++) {

        BufferedImage image = null;
        Cat cat = readApi();

        URL urlCat = new URL(cat.file);
        image = ImageIO.read(urlCat);
        File newCat;

        if(readApi().file.endsWith(".jpg")){
            newCat = new File("src\\main\\resources\\zadanie10_Koty\\cat" + i + ".jpg");
            ImageIO.write(image, "jpg", newCat);
        }else{
            newCat = new File("src\\main\\resources\\zadanie10_Koty\\cat" + i + ".gif");
            ImageIO.write(image, "gif", newCat);
        }

        System.out.println("Link: " + cat.getFile());
        System.out.println("Resolution: " + image.getWidth() + " x " + image.getHeight());
            System.out.println("Size: " + newCat.length()/1024);

        }
    }

/*

public class Main {
    public static void main(String[] args) throws IOException {

        for (int i = 0; i < 10; i++) {
            Cat cat = getRandomCatUrl();

            BufferedImage image = ImageIO.read(new URL(cat.catUrl));

            System.out.println("Resolution: " + image.getWidth() + "x" + image.getHeight());

            int lastDotIndex = cat.catUrl.lastIndexOf('.');
            String extension = cat.catUrl.substring(lastDotIndex + 1);

            File targetFile = new File("src/main/resources/losowyKot_" + i + "." + extension);

            ImageIO.write(image, extension, targetFile);

            System.out.println("Rozmiar: " + targetFile.length()/1024 + " KB");
        }

    }


    private static Cat getRandomCatUrl() throws IOException {
        URL url = new URL("https://aws.random.cat/meow");

        URLConnection connection = url.openConnection();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            String jsonText = br.readLine();
            System.out.println(jsonText);

            Cat cat = new Gson().fromJson(jsonText, Cat.class);
            System.out.println(cat.catUrl);
            return cat;
        }
    }
}
 */
}
