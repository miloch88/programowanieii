package grzegorz.zadanie5_Waluty;

public class Rates {

    private String no;
    private String effectiveDate;
    private double mid;
    private double ask;
    private double bid;

    public double getBid() {
        return bid;
    }

    public double getAsk() {
        return ask;
    }

    public String getNo() {
        return no;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public double getMid() {
        return mid;
    }

}
