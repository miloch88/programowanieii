package grzegorz.zadanie5_Waluty;

import com.google.gson.Gson;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDate;
import java.util.Scanner;

public class JSONReader {

    //Pobieramy kurs średni
    public static void getValue100UnitsMid(String url) throws IOException {
        Currency currency = readJsonFormUrl(url);

        System.out.printf("Aktualny kurs średni %s na dzień: %s to: %.4f, a 100 PLN jest warte %.2f PLN \n",
                currency.getCode(),
                currency.rates[0].getEffectiveDate(),
                currency.rates[0].getMid(),
                currency.rates[0].getMid() * 100);
    }

    //Ponieramy kurs sprzedaży
    public static void getValue100UnitsAsk(String url) throws IOException {
        Currency currency = readJsonFormUrl(url);

        System.out.printf("Aktualny kurs sprzedaży %s na dzień: %s to: %.4f, a 100 PLN jest warte %.2f PLN \n",
                currency.getCode(),
                currency.rates[0].getEffectiveDate(),
                currency.rates[0].getAsk(),
                currency.rates[0].getAsk() * 100);
    }

    //Pobieramy kurs kupna
    public static void getValue100UnitsBid(String url) throws IOException {
        Currency currency = readJsonFormUrl(url);

        System.out.printf("Aktualny kurs kupna %s na dzień: %s to: %.4f, a 100 PLN jest warte %.2f PLN \n",
                currency.getCode(),
                currency.rates[0].getEffectiveDate(),
                currency.rates[0].getBid(),
                currency.rates[0].getBid() * 100);
    }

    //Wybieramy co chcemy wiedzieć w mainie
    public static void createAction() throws IOException {

        Scanner scanner = new Scanner(System.in);
        boolean notify = false;
        String link = "http://api.nbp.pl/api/exchangerates/rates/";
        String currency = "";
        String rate = "";

        do {

            System.out.println("Wybierz swoją walutę (USD, EUR, GBP, CHF): ");
            currency = scanner.nextLine().toLowerCase();

            switch (currency) {
                case ("usd"):
                    currency = "usd";
                    notify = true;
                    break;
                case ("eur"):
                    currency = "eur";
                    notify = true;
                    break;
                case ("gbp"):
                    currency = "gbp";
                    notify = true;
                    break;
                case ("chf"):
                    currency = "chf";
                    notify = true;
                    break;
                default:
                    System.out.println("Zły wybór");
                    notify = false;
            }
        } while (notify != true);


        do {

            System.out.println("Wybierz co byś chciał wiedzieć (sredni, kupno, sprzedaz, spekulacja):");
            rate = scanner.nextLine().toLowerCase();
            switch (rate) {
                case ("sredni"):
                    rate = "a/";
                    link = link + rate + currency;
                    getValue100UnitsMid(link);
                    notify = true;
                    break;

                case ("kupno"):
                    rate = "c/";
                    link = link + rate + currency;
                    getValue100UnitsBid(link);
                    notify = true;
                    break;

                case ("sprzedaz"):
                    rate = "c/";
                    link = link + rate + currency;
                    getValue100UnitsAsk(link);
                    notify = true;
                    break;

                case ("spekulacja"):
                    rate = "c/";
                    link = link + rate + currency + "/";
                    perviousMount(link);
                    notify = true;
                    break;

                default:
                    System.out.println("Zły wybór");
                    notify = false;

            }
        } while (notify != true);
    }

    //Bawimy się w spekulanta
    public static void perviousMount(String link) throws IOException {
        LocalDate dateNow = LocalDate.now();
        LocalDate datePrevious = LocalDate.now().minusMonths(1);
        double now, previous;

        //zabezbiecznie zamiast try
//        int retryCont = 0;
//        while (now == Double.NaN || retryCont < 3) {
//            try {
//                now = readJsonFormUrl(link + dateNow).rates[0].getBid();
//            } catch (Exception e) {
//                retryCont++;
//                dateNow = dateNow.minusDays(1);
//            }
//        }

        //Zabezpieczenie na Łikęd
        try {
            now = readJsonFormUrl(link + dateNow).rates[0].getBid();
        } catch (FileNotFoundException e) {
            try {
                now = readJsonFormUrl(link + dateNow.minusDays(1)).rates[0].getBid();
            } catch (FileNotFoundException ex) {
                now = readJsonFormUrl(link + dateNow.minusDays(2)).rates[0].getBid();
            }
        }

        try {
            previous = readJsonFormUrl(link + datePrevious).rates[0].getAsk();
        } catch (FileNotFoundException e) {
            try {
                previous = readJsonFormUrl(link + datePrevious.minusDays(1)).rates[0].getAsk();
            } catch (FileNotFoundException ex) {
                previous = readJsonFormUrl(link + datePrevious.minusDays(2)).rates[0].getAsk();
            }
        }
        System.out.printf("Nasz zysk/strata miesięczna wynosi: %.2f PLN", (((100 / previous) * now)) - 100);
    }


    //Czytanie wartości ze strony http://api.nbp.pl
    public static Currency readJsonFormUrl(String url) throws IOException {

        URLConnection connection = new URL(url).openConnection();
        InputStream is = connection.getInputStream();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String jsonTxt = br.readLine();

            Gson gson = new Gson();
            Currency currency = gson.fromJson(jsonTxt, Currency.class);

            return currency;
        } finally {
            is.close();
        }
    }

}
