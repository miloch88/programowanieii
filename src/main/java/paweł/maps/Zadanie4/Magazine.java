package paweł.maps.Zadanie4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Magazine {

    private Map<ProductClass, List<Product>> listaMagazynu = new HashMap<>();

    public void dodajProduct(String nazwa, Double cena, ProductType productType, ProductClass productClass) {

        Product product = new Product(productClass, productType, cena, nazwa);

        List<Product> productList = listaMagazynu.get(productClass);
        if (productList == null) {
            productList = new ArrayList<>();
        }

        listaMagazynu.put(productClass, (List<Product>) product);
    }

    public void getType(ProductClass productClass){
        for (Map.Entry<ProductClass,List<Product>> listowanieProduktu : listaMagazynu.entrySet()){
            System.out.println("" + listowanieProduktu);
        }
    }
}
