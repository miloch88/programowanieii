package paweł.maps.Zadanie4;

public class Product {

    private ProductClass productClass;
    private ProductType productType;
    private double cena;
    private String nazwaProduktu;

    public Product(ProductClass productClass, ProductType productType, double cena, String nazwaProduktu) {
        this.productClass = productClass;
        this.productType = productType;
        this.cena = cena;
        this.nazwaProduktu = nazwaProduktu;
    }

    public ProductClass getProductClass() {
        return productClass;
    }

    public void setProductClass(ProductClass productClass) {
        this.productClass = productClass;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public Double getCena() {
        return cena;
    }

    public void setCena(Double cena) {
        this.cena = cena;
    }

    public String getNazwaProduktu() {
        return nazwaProduktu;
    }

    public void setNazwaProduktu(String nazwaProduktu) {
        this.nazwaProduktu = nazwaProduktu;
    }
}
