package paweł.maps.Zadanie1;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class RejestrObywateli {

    private Map<String, Obywatel> rejestrObywateli = new HashMap<String, Obywatel>();

    public RejestrObywateli() {
    }

    public void dodajObywatel(String pesel, String imie, String nazwisko) {
        rejestrObywateli.put(pesel, new Obywatel(pesel, imie, nazwisko));
    }

    public List<Obywatel> znajdzObywateliUrodzonchPrzed(int rok) {

        return rejestrObywateli.values()
                .stream()
                .filter(obywatel ->  rok > Integer.parseInt(obywatel.getPesel().substring(0,2))
        ).collect(Collectors.toList());

//        List<Obywatel> obywatels =
//                rejestrObywateli.values().stream().filter(obywatel -> {
//                    String rokUrodznia = obywatel.getPesel().substring(0, 2);
//                    int rokUrodzniaInt = Integer.parseInt(rokUrodznia) + 1900;
//                    if (rokUrodzniaInt < rok) {
//                        return true;
//                    }
//                    return false;
//                }).collect(Collectors.toList());
//
//        return obywatels;
    }

    public List<Obywatel> znajdzZRokuZImieniem(int rok, String imie) {
        List<Obywatel> obywatels =
                rejestrObywateli.values().stream().filter(obywatel -> {
                    String rokUrodzenia = obywatel.getPesel().substring(0, 2);
                    int rokUr = Integer.parseInt(rokUrodzenia) + 1900;
                    if (rokUr == rok && obywatel.getImie().equals(imie)) {
                        return true;
                    }
                    return false;
                }).collect(Collectors.toList());

        return obywatels;
    }

    public List<Obywatel> znajdzPoNazwisku(String nazwisko){
        return rejestrObywateli.values().stream().filter(obywatel -> obywatel.getNazwisko().equals(nazwisko))
                .collect(Collectors.toList());
    }

    public Optional<Obywatel> znajdzPoPESEL(String pesel){
       return Optional.ofNullable(rejestrObywateli.get(pesel));
    }
}
