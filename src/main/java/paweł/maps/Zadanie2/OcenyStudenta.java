package paweł.maps.Zadanie2;

import java.util.*;

public class OcenyStudenta {

    private Map<Przedmiot, List<Integer>> oceny = new HashMap<>();

    public void dodajOcene(Przedmiot przedmiot, Integer ocena){
        //pobieram oceny z mapy;
        List<Integer> ocenyZPrzedmiotu = oceny.get(przedmiot);
        //jeśli nie ma jeszcze ocen
        if(ocenyZPrzedmiotu == null){
            //tworze nową listę w której te oceny umieszczę
            ocenyZPrzedmiotu = new ArrayList<>();
        }
        //dodaję ocenę do listy
        ocenyZPrzedmiotu.add(ocena);

        //dodaje wszystkie oceny do mapy
        oceny.put(przedmiot, ocenyZPrzedmiotu);
    }

    public List<Integer> pobierzOcenyZPrzedmiotu(Przedmiot przedmiot){
        return oceny.get(przedmiot);
    }

    public Double oblicSrednia(){
        OptionalDouble srednia = oceny.values()//List<List<Integer>> // flat map -> List<Integer>
        .stream()//list.stream().mapToInt(e->e).average().getAsDouble <- obliczenie średniej z jednego przedmiotu
        .mapToDouble(list -> list.stream().mapToInt(e -> e).average().getAsDouble()).average();

        return srednia.getAsDouble();
    }

    public  Map<Przedmiot, List<Integer>> getOceny(){
        return oceny;
    }


}
