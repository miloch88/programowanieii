package paweł.maps.Zadanie2;

public class DaneStudenta {

    private String imie, nazwisko, indeks;
    private OcenyStudenta ocenyStudenta = new OcenyStudenta();

    public DaneStudenta(String imie, String nazwisko, String indeks) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.indeks = indeks;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getIndeks() {
        return indeks;
    }

    public void setIndeks(String indeks) {
        this.indeks = indeks;
    }

    public void setOcenyStudenta(OcenyStudenta ocenyStudenta){
        this.ocenyStudenta = ocenyStudenta;
    }

    public OcenyStudenta getOcenyStudenta() {
        return ocenyStudenta;
    }
}
