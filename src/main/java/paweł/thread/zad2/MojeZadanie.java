package paweł.thread.zad2;

public class MojeZadanie implements Runnable {

    private String dodatkowyTeskt;

    public void setDodatkowyTeskt(String dodatkowyTeskt) {
        this.dodatkowyTeskt = dodatkowyTeskt;
    }

    @Override
    public void run() {
        int i = 0;

        try {
            while (true) {
                System.out.println((i++) + ". Hello World " + dodatkowyTeskt);
                Thread.sleep(5000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
