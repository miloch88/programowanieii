/*
2. Stwórz aplikację a w niej wątek który raz na 5 sekund wypisuje na ekran:

1. Hello World! - tekst
2. Hello World!! - tekst
3. Hello World!!! - tekst

Główny wątek powinien przyjmować tekst który ma występować po hello world. np. jeśli wpisze:
"Michałki"

wątek powinien wypisywać:
...
4. Hello World!!!! - Michałki
...

https://bitbucket.org/javagda15/threadingnexercises/src/master/src/main/java/pl/sda/javagda15/exercises/threading/zad2_loop/
 */

package paweł.thread.zad2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args)  {

        MojeZadanie zadanie = new MojeZadanie();
        Thread watek = new Thread(zadanie);
        watek.start();

        Scanner scanner = new Scanner(System.in);
        String linia;
        do{
            //wczytuje linie
            linia = scanner.nextLine();

            //Przerywa wątek
            if(linia.equalsIgnoreCase("int")) {
                watek.interrupt();
                continue;
            }


            //przekazuje ją do zadania
            zadanie.setDodatkowyTeskt(linia);
        }while (!linia.equalsIgnoreCase("quit"));

        if(watek.isAlive()){
            watek.stop();
        }
    }
}
