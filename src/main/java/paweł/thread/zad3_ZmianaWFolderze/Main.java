/*
3. Stwórz aplikację która co sekundę (w drugim wątku) sprawdza czy w katalogu pojawił się
nowy plik. Jeśli pojawił się nowy plik lub jakiś zniknął, to wypisuje stosowny komunikat na
ekran. Drugi wątek (główny) ma przyjmować w linii poleceń adres katalogu który jest
sprawdzany (możemy zmienic adres czytanego katalogu).
Podpowiedź:
File folder = ​new ​File(​"."​);
folder.listFiles() // <- instrukcja zwraca tablicę plików w podanym folderze

To nie jest zrobione do końca
4. Stwórz aplikację która cyklicznie czyta plik i jeśli doszła jakakolwiek zmiana w pliku, to
wypisuje ją na ekran. Drugi wątek ma przyjmować adres pliku który jest sprawdzany
(możemy zmienić adres czytanego pliku.

https://bitbucket.org/javagda15/threadingnexercises/src/master/src/main/java/pl/sda/javagda15/exercises/threading/zad3_folderCheck/
 */

package paweł.thread.zad3_ZmianaWFolderze;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        FolderChecker folderChecker = new FolderChecker();

        Thread watek = new Thread(folderChecker);

        watek.start();

        Scanner scanner = new Scanner(System.in);
        String linia;

        do{
            linia = scanner.nextLine();

            folderChecker.setSciezkaSprawdzana(linia);
        }while (!linia.equalsIgnoreCase("quit"));

    }
}
