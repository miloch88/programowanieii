package paweł.thread.zad3_ZmianaWFolderze;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public class FolderChecker implements Runnable {

    private String sciezkaSprawdzana;
    private Set<File> fileSet = new HashSet<>();

    public FolderChecker() {
        this.sciezkaSprawdzana = ".";
    }

    public void setSciezkaSprawdzana(String sciezkaSprawdzana) {
        this.sciezkaSprawdzana = sciezkaSprawdzana;
    }

    @Override
    public void run() {

        for (int i = 0; i < 1000; i++){
            try{
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //folder sprawdzany
            File sprawdzany = new File(sciezkaSprawdzana);

            //tworzę zbiór pików (obecnych)
            Set<File> fileNames = new HashSet<>();
            for (File f : sprawdzany.listFiles()) {
                fileNames.add(f);
            }

            //sprawdzam rozmiary zbuorów
            if(fileSet.size() != fileNames.size()){
                System.out.println("Różna ilość plików");
            }

            Set<File> kopia = new HashSet<>(fileNames);

            //pozostawi mi w zbiorze fileNames tylko nowe pliki
            fileNames.removeAll(fileSet);

            //dodanie nowych plików do kolecji fileSet
            for (File plik : fileNames) {
                System.out.println("Dodano nowy plik: " + plik.getName());
                fileSet.add(plik);
            }

            Set<File> kopiaFileSet = new HashSet<>(fileSet);
            kopiaFileSet.removeAll(kopia);

            //usunięcie usuniętych plików
            for (File plik : kopiaFileSet) {
                System.out.println("Usunięto pliki: " + plik.getName());
                fileSet.remove(plik);

            }
        }

    }
}
