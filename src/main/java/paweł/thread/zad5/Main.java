/*
5. Stwórz aplikację "timery". Wątek główny przyjmuje w linii liczbę. Liczba oznacza czas do
wybudzenia. Po wpisaniu liczby (np. 50000) aplikacja ma startować nowy wątek, który ma
się wybudzić za ten czas (np. za 50 s.) i wypisze komunikat: (To Twój timer 50 s. - wake up!)
5B*. Na komendę quit główny wątek ma opuścić pętle i poczekać na wszystkie rozpoczęte
zadania (join()). Jeśli jakieś trwają to zatrzymaj wykonanie i wypisz ile wątków pracuje (zlicz
tylko wątki isAlive()) a następnie poczekaj na ich zakończenie.
 */

package paweł.thread.zad5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws InterruptedException {


        Scanner scanner = new Scanner(System.in);
        String linia;
        Thread watek;

        do {
            linia = scanner.nextLine();

            final Long time = Long.parseLong(linia);
            watek = new Thread(() -> {
                try {
                    Thread.sleep(time);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Timer " + time + " koniec");
            });
            watek.start();

        } while (!linia.equalsIgnoreCase("quit"));

        watek.interrupt();
        watek.join();

    }
}
