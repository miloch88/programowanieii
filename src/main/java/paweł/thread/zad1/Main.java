/*
1. Stwórz aplikację, w niej uruchom wątek, którego zadaniem jest po 30 sekundach wypisać komunikat "Juhuuuu!".
Po tej czynności aplikacja ma się zamknąć.

https://bitbucket.org/javagda15/threadingnexercises/src/master/src/main/java/pl/sda/javagda15/exercises/threading/zad1_30sek/
 */

package paweł.thread.zad1;

public class Main {
    public static void main(String[] args) {

        Thread watek = new Thread(() -> {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(" Juppppiiii");
        });

        watek.start();

    }
}
