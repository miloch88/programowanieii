package adam.manager.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class BufferedFileReader extends AbstractFileReader {

    @Override
    public List<String> read(String path) throws IOException {
        File file = getFile(path);

        List<String> result = new ArrayList<>();

        try(BufferedReader br = new BufferedReader(new FileReader(file))){
            String line = br.readLine();
            while (line != null){
                line = br.readLine();
                result.add(line);
            }
        }
        return result;
    }
}
