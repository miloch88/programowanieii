package adam.manager.model;

import adam.manager.generators.GeneratorType;
import adam.manager.generators.PasswordGeneratorFacade;

public class PasswordFacade {

    private PasswordGeneratorFacade passwordFacade;

    public PasswordEntry generatePassword(String website, String login, GeneratorType strategy, int length){
        String passoword = passwordFacade.getPassword(length,strategy);
        return new PasswordEntry(website, passoword, login);
    }
}
