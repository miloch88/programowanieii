package adam.manager;

import adam.manager.generators.GeneratorType;
import adam.manager.generators.PasswordGeneratorFacade;

public class Main {
    public static void main(String[] args) {

        PasswordGeneratorFacade passwordGeneratorFacade = new PasswordGeneratorFacade();

        System.out.println(passwordGeneratorFacade.getPassword(25, GeneratorType.UUID));


    }
}
