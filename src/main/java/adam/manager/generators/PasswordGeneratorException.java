package adam.manager.generators;

public class PasswordGeneratorException extends RuntimeException {

    public PasswordGeneratorException(String message){
        super(message);
    }
}
