package adam.passwordManager;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.stream.Collectors;

public class EncryptionService {

    private static final String ALGORITHM = "AES";

    public static String encrypt(String encryptionKey, String toEncode) {
        try {
            byte[] key = encryptionKey.getBytes(StandardCharsets.UTF_8);
            SecretKeySpec secretKey = new SecretKeySpec(key, ALGORITHM);
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            byte[] bytes = cipher.doFinal(toEncode.getBytes(StandardCharsets.UTF_8));
            System.out.println(new String(bytes));
            System.out.println();
            return Base64.encodeBase64String(bytes);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

//    public static String decrypt(String encryptionKey, String toDecode) {
////        try {
////            toDecode = new String(Base64.decodeBase64(toDecode.getBytes(StandardCharsets.UTF_8)));
////            byte[] key = encryptionKey.getBytes(StandardCharsets.UTF_8);
////            SecretKeySpec secretKey = new SecretKeySpec(key, ALGORITHM);
////            Cipher cipher = Cipher.getInstance(ALGORITHM);
////            cipher.init(Cipher.DECRYPT_MODE, secretKey);
////
////            return new String(cipher.doFinal(toDecode.getBytes(StandardCharsets.UTF_8)));
////        } catch (Exception e) {
////            e.printStackTrace();
////            return "";
////        }
////    }

    public static String decrypt(String encryptionKey, String toDecode) {
        try {
            byte[] key = encryptionKey.getBytes(StandardCharsets.UTF_8);
            SecretKeySpec secretKey = new SecretKeySpec(key, ALGORITHM);
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            byte[] decordedValue = Base64.decodeBase64(toDecode);
            return new String(cipher.doFinal(decordedValue));
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String[] encrypt(String encryptionKey, String[] toEncode) {

        return Arrays.stream(toEncode).map(s -> EncryptionService.encrypt(encryptionKey, s))
                .collect(Collectors.toList())
                .toArray(new String[toEncode.length]);

    }

    public static String[] decrypt(String encryptionKey, String[] toDecode) {
        return Arrays.stream(toDecode).map(s -> EncryptionService.decrypt(encryptionKey, s))
                .collect(Collectors.toList())
                .toArray(new String[toDecode.length]);

    }
}
