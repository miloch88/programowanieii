package adam.passwordManager.Menu;

import adam.passwordManager.PasswordEntry;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import static adam.passwordManager.NewPassword.newPassword;
import static adam.passwordManager.PasswordFile.readFile;

public class Menu {

    static Scanner scanner = new Scanner(System.in);

    public static void startProgram() throws IOException {
        System.out.println("Co chcesz zrobić? \n1. Wygnereować nowe hasło: \n2. Wydobyć hasło do: ");

        int choose;

        switch (choose = scanner.nextInt()) {
            case (1):
                scanner.nextLine();
                newPassword();
                break;
            case (2):
                scanner.nextLine();
                searchPassword();
                break;
        }
    }


    private static void searchPassword() {

        List<PasswordEntry> passwordEntries = readFile();

        System.out.println("Podaj nazwę strony: ");
        String nameWebSite = scanner.nextLine();

        for (PasswordEntry pe : passwordEntries) {
            if (pe.website.equals(nameWebSite.toLowerCase())) {
                System.out.println(pe.password);
            }
        }
    }
}