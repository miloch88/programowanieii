package adam.passwordManager.Menu;

public enum MenuState {

    MAIN_MENU,
    CREATE_NEW_PASSWORD,
    GET_PASSWORD;

}
