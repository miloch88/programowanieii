package adam.passwordManager;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class PasswordFile {

    static List<PasswordEntry> passwordEntries = new ArrayList<>();
    static File file = new File("src\\main\\resources\\passwordManager\\Passwords.txt");

    public static void writeToFile(String password) throws IOException {


        if (!file.exists()) {
            file.createNewFile();
        }
        FileWriter fw = new FileWriter(file, true);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(password + "\n");
        bw.close();

        System.out.println("Done");

    }

    public static List<PasswordEntry> readFile() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                String[] readLine = line.split(";");

                PasswordEntry passwordEntry = new PasswordEntry(readLine[0], readLine[1]);
                passwordEntries.add(passwordEntry);
            }

            br.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return passwordEntries;
    }
}

