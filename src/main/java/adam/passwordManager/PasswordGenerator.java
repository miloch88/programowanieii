package adam.passwordManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PasswordGenerator {

    private static final String lower = "abcdefghijklmnopqrstuwxyz";
    private static final String upper = "ABCDEFGHIJKLMNOPQRSTUWXYZ";
    private static final String digits = "0123456789";
    private static final String punctuation = "!@#$%&*()+-=[]/?><";

    private boolean useLower;
    private boolean useUpper;
    private boolean useDigits;
    private boolean usePunctuation;


    public PasswordGenerator() {
        throw new UnsupportedOperationException("Empty constructor is not supported.");
    }

    private PasswordGenerator(PasswordGeneratorBuilder builder){
        this.useLower = builder.useLower;
        this.useUpper = builder.useUpper;
        this.useDigits = builder.useDigits;
        this.usePunctuation = builder.usePunctuation;
    }
    public static class PasswordGeneratorBuilder{

        private boolean useLower;
        private boolean useUpper;
        private boolean useDigits;
        private boolean usePunctuation;

        public PasswordGeneratorBuilder() {
            this.useLower = false;
            this.useUpper = false;
            this.useDigits = false;
            this.usePunctuation = false;
        }

        public PasswordGeneratorBuilder setUseLower(boolean useLower) {
            this.useLower = useLower;
            return this;
        }

        public PasswordGeneratorBuilder setUseUpper(boolean useUpper) {
            this.useUpper = useUpper;
            return this;
        }

        public PasswordGeneratorBuilder setUseDigits(boolean useDigits) {
            this.useDigits = useDigits;
            return this;
        }

        public PasswordGeneratorBuilder setUsePunctuation(boolean usePunctuation) {
            this.usePunctuation = usePunctuation;
            return this;
        }

        public PasswordGenerator bulid() {
            return new PasswordGenerator(this);
        }
    }

    public String generate(int length) {
        if(length <= 0){
            return "";
        }

        StringBuilder password = new StringBuilder(length);
        Random random = new Random();

        List<String> charCategories = new ArrayList<>(4);
        if(useLower){
            charCategories.add(lower);
        }
        if(useUpper){
            charCategories.add(upper);
        }
        if(useDigits){
            charCategories.add(digits);
        }
        if(usePunctuation){
            charCategories.add(punctuation);
        }

        for (int i = 0; i < length; i++) {
            String charCategory = charCategories.get(random.nextInt(charCategories.size()));
            int position = random.nextInt(charCategory.length());
            password.append(charCategory.charAt(position));
        }

        return new String(password);
    }
}
