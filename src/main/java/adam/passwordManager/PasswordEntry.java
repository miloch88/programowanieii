package adam.passwordManager;

public class PasswordEntry {

    public String website;
    public String password;

    public PasswordEntry(String website, String password) {
        this.website = website;
        this.password = password;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "PasswordEntry{" +
                "website='" + website + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
