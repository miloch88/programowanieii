package adam.passwordManager;

import java.io.IOException;
import java.util.Scanner;

import static adam.passwordManager.PasswordFile.writeToFile;

public class NewPassword {

    public static void newPassword() throws IOException {

        PasswordGenerator passwordGenerator = new PasswordGenerator.PasswordGeneratorBuilder()
            .setUseDigits(true)
            .setUseLower(true)
            .setUseUpper(true)
            .setUsePunctuation(true)
            .bulid();

        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj nazwę strony: ");
        String nazwa = scanner.nextLine();

        String password = passwordGenerator.generate(16);

        writeToFile(nazwa+";"+password);

    }
}
